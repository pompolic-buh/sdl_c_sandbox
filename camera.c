#include "camera.h"

static camera_t camera;

int CAM_Init(void )
{
	camera.position.x = 0;
	camera.position.y = 0;
	camera.position.z = 0;

	camera.rotation.x = 45;
	camera.rotation.y = 0;
	camera.rotation.z = 0;

	camera.distance = 24;

	return 0;
}

camerarot_t CAM_GetCameraRotation(void )
{
	return camera.rotation;
}

void CAM_RotateCamera(GLdouble xrot, GLdouble yrot, GLdouble zrot)
{
	camera.rotation.x += xrot;
	camera.rotation.y += yrot;
	camera.rotation.z += zrot;
}

camerapos_t CAM_GetCameraPosition()
{
    return camera.position;
}

void CAM_MoveCamera(GLdouble xdelta, GLdouble ydelta, GLdouble zdelta)
{
    camera.position.x += xdelta;
    camera.position.y += ydelta;
    camera.position.z += zdelta;
}

void CAM_Quit(void )
{

}

GLdouble CAM_GetCameraDistance(void)
{
    return camera.distance;
}

void CAM_UpdateCameraDistance(GLdouble delta)
{
    camera.distance += delta;
}

