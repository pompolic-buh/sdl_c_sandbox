#ifndef PLAYER_H
#define PLAYER_H

#include <GL/gl.h>

typedef struct playerpos_
{
	GLdouble x, y, z;
} playerpos_t;

typedef struct playerfacing_
{
	GLdouble xrot, yrot, zrot;
} playerfacing_t;

typedef struct playerstate_
{
	playerpos_t position;
	playerfacing_t facing;
	double health;
} playerstate_t;

// Initializes player. Returns 0 on success
int P_InitPlayer(void);

// Return generic player state
playerstate_t P_GetPlayerState(void);

// Return current player position
playerpos_t P_GetPlayerPos(void);
// Add or subtract value from player position
playerpos_t P_UpdatePlayerPos(GLdouble xdelta, GLdouble ydelta, GLdouble zdelta);
// FIXME: orthogonality is a what
playerpos_t P_SetPlayerHeight(GLdouble newy);

// Return current player orientation
playerfacing_t P_GetPlayerFacing(void);
// Add or subtract value from player orientation
playerfacing_t P_UpdatePlayerFacing(GLdouble xrot, GLdouble yrot, GLdouble zrot);

// Return current player health
float P_GetPlayerHealth(void);
// Add or subtract value from player health
float P_UpdatePlayerHealth(float delta);

// Returns nonzero if the two are not equal, 0 if they are
// int P_ComparePlayerPos(playerpos_t left, playerpos_t right);

// Tell OpenGL to draw the player
void P_DrawPlayer(void);

#endif // PLAYER_H
