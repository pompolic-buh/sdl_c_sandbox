#include <math.h>
// for rounding
#include <stdlib.h>
//FIXME: DEBUG
#include <stdio.h>
#include <SDL/SDL_image.h>
#include <time.h>


#include "level.h"
#include "player.h"

#ifdef DEBUG_HUD
#include "debug_hud.h"
#endif

#define MOVING_DIAG ((TILES_NUM_W) + (TILES_NUM_H) - 1)
#define MOVING_HORIZ_OR_VERT (TILES_NUM_W)

#define TILEBUF_SIZE ((TILES_NUM_W)*(TILES_NUM_H))

#define VERTEX_HI_FAR_RIGHT		0
#define VERTEX_HI_FAR_LEFT		1
#define VERTEX_HI_NEAR_LEFT		2
#define VERTEX_HI_NEAR_RIGHT	3
#define VERTEX_LO_FAR_RIGHT		4
#define VERTEX_LO_FAR_LEFT		5
#define VERTEX_LO_NEAR_LEFT		6
#define VERTEX_LO_NEAR_RIGHT	7
// #define TILEBUF_SIZE 256


// row, column
static L_tile_t tiles[TILEBUF_SIZE];
static SDL_Surface *tile_tex = NULL, *special_tex = NULL;
static GLuint texIndex;
/* display list for tile */
static GLuint tileList;

static SDL_PixelFormat *spec_format;

static GLuint columnIndices[] = {
	VERTEX_HI_FAR_RIGHT, VERTEX_HI_FAR_LEFT, VERTEX_HI_NEAR_LEFT, VERTEX_HI_NEAR_RIGHT,		/* top    */
	VERTEX_HI_NEAR_RIGHT, VERTEX_HI_NEAR_LEFT, VERTEX_LO_NEAR_LEFT, VERTEX_LO_NEAR_RIGHT,	/* front  */
	VERTEX_HI_FAR_RIGHT, VERTEX_HI_NEAR_RIGHT, VERTEX_LO_NEAR_RIGHT, VERTEX_LO_FAR_RIGHT,	/* right  */
	VERTEX_HI_FAR_LEFT, VERTEX_HI_FAR_RIGHT, VERTEX_LO_FAR_RIGHT, VERTEX_LO_FAR_LEFT,		/* back   */
	VERTEX_HI_NEAR_LEFT, VERTEX_HI_FAR_LEFT, VERTEX_LO_FAR_LEFT, VERTEX_LO_NEAR_LEFT,		/* left	  */
	VERTEX_LO_NEAR_RIGHT, VERTEX_LO_NEAR_LEFT, VERTEX_LO_FAR_LEFT, VERTEX_LO_FAR_RIGHT		/* bottom */
};

static GLdouble texCoords[24][2] = {
	{1.0, 0.0}, {0.0, 0.0}, {0.0, 1.0}, {1.0, 1.0},
	{1.0, 0.0}, {0.0, 0.0}, {0.0, 1.0}, {1.0, 1.0},
	{1.0, 0.0}, {0.0, 0.0}, {0.0, 1.0}, {1.0, 1.0},
	{1.0, 0.0}, {0.0, 0.0}, {0.0, 1.0}, {1.0, 1.0},
	{1.0, 0.0}, {0.0, 0.0}, {0.0, 1.0}, {1.0, 1.0},
	{1.0, 0.0}, {0.0, 0.0}, {0.0, 1.0}, {1.0, 1.0}
};

void L_Init()
{
	//FIXME: put random generator in separate files
// 	srand48(53);
//	srand48(time(NULL));
//  "portability" "fixes"
	srand(time(NULL));


	/* Generate display list for tile */
	tileList = glGenLists(1);

	glNewList(tileList, GL_COMPILE);
	glBegin(GL_QUADS);

	/* Bottom right, top right, top left, bottom left */
	glTexCoord2d(1.0, 1.0); glVertex3d(  TILE_SIZE / 2.0 , 0.0,   TILE_SIZE / 2.0 );
	glTexCoord2d(1.0, 0.0); glVertex3d(  TILE_SIZE / 2.0 , 0.0, -(TILE_SIZE / 2.0));
	glTexCoord2d(0.0, 0.0); glVertex3d(-(TILE_SIZE / 2.0), 0.0, -(TILE_SIZE / 2.0));
	glTexCoord2d(0.0, 1.0); glVertex3d(-(TILE_SIZE / 2.0), 0.0,   TILE_SIZE / 2.0 );

	glEnd();
	glEndList();

	spec_format = malloc(sizeof(SDL_PixelFormat));

	if(tile_tex) return;

// 	special_tex = SDL_CreateRGBSurface(SDL_SRCALPHA, 512, 512, 32, 0x0000FF00, 0x00FF0000, 0x000000FF, 0xFF000000);

	tile_tex = IMG_Load("square.png");
// 	tile_tex = IMG_Load("a_face.bmp");
 	//tile_tex = IMG_Load("lunavisbg4_256.png");
	if(!tile_tex)
	{
		fprintf(stderr, "L_PrepareTexture: Failed to load texture! %s\n", IMG_GetError());
	}

	//FIXME: used a uninitialized pointer, what the actual
	*spec_format = *(tile_tex->format);

	spec_format->Rmask = 0x00FF0000;
	spec_format->Gmask = 0x0000FF00;
	spec_format->Bmask = 0x000000FF;
	spec_format->Amask = 0xFF000000;

	special_tex = SDL_ConvertSurface(tile_tex, spec_format, SDL_SRCALPHA);

	//TODO: might need RGBA -> BGRA conversion + flipping


	//FIXME: if SDL_LockSurface() returns because the tex is unable to be locked, this will crash
	if(tile_tex)
		SDL_LockSurface(tile_tex);
	else
		return;
	if(special_tex)
		SDL_LockSurface(special_tex);
	else
		return;

	//generate/bind textures
	glGenTextures(1, &texIndex);
	glBindTexture(GL_TEXTURE_2D, texIndex);

	//for the hexagon texture
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tile_tex->w, tile_tex->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, tile_tex->pixels);

	//for everything else
// 	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tile_tex->w, tile_tex->h, 0, GL_BGR, GL_UNSIGNED_BYTE, tile_tex->pixels);

    //GL_BGR is undefined on windows apparently
 	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tile_tex->w, tile_tex->h, 0, GL_RGB, GL_UNSIGNED_BYTE, tile_tex->pixels);
// 	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, special_tex->w, special_tex->h, 0, GL_BGR, GL_UNSIGNED_BYTE, special_tex->pixels);

	//texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	SDL_UnlockSurface(special_tex);
	SDL_UnlockSurface(tile_tex);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void L_Generate(void )
{
	GLdouble height;
	GLdouble right, left;
	GLdouble far, near;
	int i, j, k;

	for (i = 0; i < TILES_NUM_H; ++i)
	{
		for (j = 0; j < TILES_NUM_W; ++j)
		{
//			height = (GLdouble) (drand48() * 5.0);
            //"portability" "fixes"
			height = (GLdouble) (((rand() % 10) /10.0) * 5.0);
			right = ((GLdouble) j - TILES_NUM_W/2) + (TILE_SIZE / 2.0);
			left = ((GLdouble) j - TILES_NUM_W/2) - (TILE_SIZE / 2.0);
			far = ((GLdouble) i - TILES_NUM_H/2) - (TILE_SIZE / 2.0);
			near = ((GLdouble) i - TILES_NUM_H/2) + (TILE_SIZE / 2.0);

			tiles[i*TILES_NUM_H+j].z = i;
			tiles[i*TILES_NUM_H+j].x = j;
			tiles[i*TILES_NUM_H+j].h = height;
			tiles[i*TILES_NUM_H+j].show = 1;

			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_FAR_RIGHT].x = right;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_FAR_RIGHT].y = height;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_FAR_RIGHT].z = far;

			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_FAR_LEFT].x = left;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_FAR_LEFT].y = height;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_FAR_LEFT].z = far;

			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_NEAR_LEFT].x = left;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_NEAR_LEFT].y = height;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_NEAR_LEFT].z = near;

			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_NEAR_RIGHT].x = right;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_NEAR_RIGHT].y = height;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_HI_NEAR_RIGHT].z = near;

			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_FAR_RIGHT].x = right;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_FAR_RIGHT].y = 0.0;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_FAR_RIGHT].z = far;

			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_FAR_LEFT].x = left;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_FAR_LEFT].y = 0.0;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_FAR_LEFT].z = far;

			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_NEAR_LEFT].x = left;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_NEAR_LEFT].y = 0.0;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_NEAR_LEFT].z = near;

			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_NEAR_RIGHT].x = right;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_NEAR_RIGHT].y = 0.0;
			tiles[i*TILES_NUM_H+j].vertices[VERTEX_LO_NEAR_RIGHT].z = near;
		}
	}

}

// Rounds player position to nearest integer divisible by TILE_SIZE
playerpos_t L_RoundPlayerPosition(playerpos_t player)
{
    playerpos_t result;

	GLdouble x = TILE_SIZE * round(player.x / TILE_SIZE);
	GLdouble y = TILE_SIZE * round(player.y / TILE_SIZE);
	GLdouble z = TILE_SIZE * round(player.z / TILE_SIZE);

	result.x = x;
	result.y = y;
	result.z = z;

    return result;
}

// Tests whether a given tile is out of drawing range. Assumes player position is rounded to nearest integer
int L_OutOfRadius(L_tile_t *tile, playerpos_t player, GLdouble xdir, GLdouble zdir)
{
    if (xdir >= 0.0 && zdir >= 0.0)
    {
        return (tile->vertices[3].x < player.x - TILE_DRAW_RADIUS*(TILE_SIZE) ||  // one of the tile's leftmost vertices
                tile->vertices[0].z < player.z - TILE_DRAW_RADIUS*(TILE_SIZE));	 // or one of the tile's farthest vertices is out of range
    }
    else if (xdir >= 0.0 && zdir < 0.0)
    {
        return (tile->vertices[3].x < player.x - TILE_DRAW_RADIUS*(TILE_SIZE) ||
                tile->vertices[1].z > player.z + TILE_DRAW_RADIUS*(TILE_SIZE));	// same as above but with the closest vertex
    }
    else if (xdir < 0.0 && zdir >= 0.0)
    {
        return (tile->vertices[2].x > player.x + TILE_DRAW_RADIUS*(TILE_SIZE) ||
                tile->vertices[0].z < player.z - TILE_DRAW_RADIUS*(TILE_SIZE));
    }
    else // if (xdir < 0.0 && zdir < 0.0)
    {
        return (tile->vertices[2].x > player.x + TILE_DRAW_RADIUS*(TILE_SIZE) ||
                tile->vertices[1].z > player.z + TILE_DRAW_RADIUS*(TILE_SIZE));
    }

    return 0;
}

void L_CullTiles(playerpos_t player_ur)
{
    int i;

    playerpos_t player = L_RoundPlayerPosition(player_ur);
// 	playerpos_t player = L_RoundPlayerPositionInt(player_ur);
	static playerpos_t lastpos;

	GLdouble xdir = player.x - lastpos.x;
	GLdouble zdir = player.z - lastpos.z;

    for(i = 0; i < TILEBUF_SIZE ; ++i)
    {
        if (L_OutOfRadius(&tiles[i], player, xdir, zdir))
        {
            tiles[i].show = 0;
        }
    }

    lastpos = player;
}

void L_WipeTiles()
{
	int i;

	for(i = 0; i < TILEBUF_SIZE ; ++i)
    {
        tiles[i].show = 0;
    }
}

// TODO: if needed, rewrite so it doesn't change every tile in the buffer on every frame
// FIXME: I'm not sure why this needs the player position as an argument
void L_GenTiles(playerpos_t player_ur)
{
    static L_tile_t *next_tile = tiles;

    playerpos_t player = L_RoundPlayerPosition(player_ur);
	static playerpos_t lastpos;


	GLdouble sign_of_xdir = player.x - lastpos.x;
	GLdouble sign_of_zdir = player.z - lastpos.z;

	int xdir = (signbit(sign_of_xdir) ? -1 : ((sign_of_xdir != 0) ? 1 : 0));
	int zdir = (signbit(sign_of_zdir) ? -1 : ((sign_of_zdir != 0) ? 1 : 0));

		int i, j;

		L_WipeTiles();	//FIXME: entirely unnecessary

		for(i = 0; i < TILES_NUM_H; ++i)
		{
			for(j = 0; j < TILES_NUM_W; ++j)
			{
//				tiles[i*TILES_NUM_H+j].h = drand48();
                //TODO: put this into an ifdef because this modulo business is terrible
                //"portability" "fixes"
				tiles[i*TILES_NUM_H+j].h = (rand() % 10) / 10.0;
				//x,z coordinates of the current tile's centerpoints. used to determine its color
				GLdouble zcenter = ((tiles[i*TILES_NUM_H+j].vertices[0].z + tiles[i*TILES_NUM_H+j].vertices[1].z)/2);
				GLdouble xcenter = ((tiles[i*TILES_NUM_H+j].vertices[0].x + tiles[i*TILES_NUM_H+j].vertices[3].x)/2);
				//GLdouble bcomponent = 1-(sqrt((xcenter*xcenter)+(zcenter*zcenter))) * 0.09;

				tiles[i*TILES_NUM_H+j].show = 1;

				//upper right
// 				tiles[i*TILES_NUM_H+j].vertices[0].x = player.x + (i - TILES_NUM_W/2 + 1) * TILE_SIZE;
				tiles[i*TILES_NUM_H+j].vertices[0].x = TILE_SIZE / 2.0;
				tiles[i*TILES_NUM_H+j].vertices[0].y = tiles[i*TILES_NUM_H+j].h;
// 				tiles[i*TILES_NUM_H+j].vertices[0].z = player.z + (j - TILES_NUM_H/2 ) * TILE_SIZE;
				tiles[i*TILES_NUM_H+j].vertices[0].z = -(TILE_SIZE / 2.0);

				//lower right
// 				tiles[i*TILES_NUM_H+j].vertices[1].x = player.x + (i - TILES_NUM_W/2 + 1) * TILE_SIZE;
				tiles[i*TILES_NUM_H+j].vertices[1].x = TILE_SIZE / 2.0;
				tiles[i*TILES_NUM_H+j].vertices[1].y = tiles[i*TILES_NUM_H+j].h;
// 				tiles[i*TILES_NUM_H+j].vertices[1].z = player.z + (j - TILES_NUM_H/2 + 1) * TILE_SIZE;
				tiles[i*TILES_NUM_H+j].vertices[1].z = TILE_SIZE / 2.0;

				//lower left
// 				tiles[i*TILES_NUM_H+j].vertices[2].x = player.x + (i - TILES_NUM_W/2 ) * TILE_SIZE;
				tiles[i*TILES_NUM_H+j].vertices[2].x = -(TILE_SIZE / 2.0);
				tiles[i*TILES_NUM_H+j].vertices[2].y = tiles[i*TILES_NUM_H+j].h;
// 				tiles[i*TILES_NUM_H+j].vertices[2].z = player.z + (j - TILES_NUM_H/2 + 1) * TILE_SIZE;
				tiles[i*TILES_NUM_H+j].vertices[2].z = TILE_SIZE / 2.0;

				//upper left
// 				tiles[i*TILES_NUM_H+j].vertices[3].x = player.x + (i - TILES_NUM_W/2 ) * TILE_SIZE;
				tiles[i*TILES_NUM_H+j].vertices[3].x = -(TILE_SIZE / 2.0);
				tiles[i*TILES_NUM_H+j].vertices[3].y = tiles[i*TILES_NUM_H+j].h;
// 				tiles[i*TILES_NUM_H+j].vertices[3].z = player.z + (j - TILES_NUM_H/2 ) * TILE_SIZE;
				tiles[i*TILES_NUM_H+j].vertices[3].z = -(TILE_SIZE / 2.0);

				//farther along the X axis, the red component of the tiles' color is higher; same goes for the green component for farther along Z
				tiles[i*TILES_NUM_H+j].colors[0] = xcenter*0.01;
				tiles[i*TILES_NUM_H+j].colors[1] = zcenter*0.01;
				tiles[i*TILES_NUM_H+j].colors[2] = 0.0;

			}
		}

	lastpos = player;
}


void L_DrawTiles(void )
{
    int i,j;
	int curr_vert, curr_tex;
	static double ang = 0.0;
	playerpos_t player = L_RoundPlayerPosition(P_GetPlayerPos());

	glPushMatrix();

	//uncomment this for textures
	glBindTexture(GL_TEXTURE_2D, texIndex);

// 	glMatrixMode(GL_TEXTURE);
	//glRotated(ang, 0.0, 0.0, 1.0);

	if(ang > 360.0)
		ang = 0.0;
	else
		ang += 1.0;

	glMatrixMode(GL_MODELVIEW);

	glPushMatrix();
// 	glTranslated(player.x - (TILES_NUM_W/2.0) * TILE_SIZE, 0.0, player.z - (TILES_NUM_H/2.0) * TILE_SIZE);
//
//
// 	for(i = 0; i < TILES_NUM_W; ++i)
// 	{
// 		for(j = 0; j < TILES_NUM_H; ++j)
// 		{
// // 			glTranslated(player.x + (i - TILES_NUM_W/2.0) * TILE_SIZE, 0.0, player.z + (j - TILES_NUM_H/2.0) * TILE_SIZE);
// 			glCallList(tileList);
// 			glTranslated(0,0,TILE_SIZE);
//
// 		}
// 		glTranslated(TILE_SIZE,0,0);
// 		glTranslated(0,0,-TILES_NUM_H * TILE_SIZE);
// 	}
//
// 	glPopMatrix();
// 	glLoadIdentity();
// 	glTranslatef(0.0, 0.0, -24.0);
// 	glRotatef(45.0, 1.0, 0.0, 0.0);

    glBegin(GL_QUADS);

// 	glTexCoord2d(1.0, 1.0); glVertex3d(  tiles[31].vertices[VERTEX_HI_FAR_RIGHT].x , 0.0,  tiles[31].vertices[VERTEX_HI_FAR_RIGHT].z );
// 	glTexCoord2d(1.0, 0.0); glVertex3d(  TILE_SIZE / 2.0 , 0.0, -(TILE_SIZE / 2.0));
// 	glTexCoord2d(0.0, 0.0); glVertex3d(-(TILE_SIZE / 2.0), 0.0, -(TILE_SIZE / 2.0));
// 	glTexCoord2d(0.0, 1.0); glVertex3d(-(TILE_SIZE / 2.0), 0.0,   TILE_SIZE / 2.0 );

    for(i = 0; i < TILEBUF_SIZE ; ++i)
    {
        if (tiles[i].show)
        {
			//glColor4d(tiles[i].colors[0], tiles[i].colors[1], tiles[i].colors[2], 1.0);


			for (j = 0; j < 24; ++j)
			{
				curr_tex = j % 4;
				curr_vert = columnIndices[j];
				glTexCoord2d(texCoords[curr_tex][0], texCoords[curr_tex][1]);
// 				glTexCoord2d(curr_tex == 0 || curr_tex == 3 ? 1.0f : 0.0f, curr_tex < 2 ? 1.0f : 0.0f);
				glVertex3d(tiles[i].vertices[curr_vert].x, tiles[i].vertices[curr_vert].y, tiles[i].vertices[curr_vert].z);
			}
			//upper vertices
//             for(j = 0; j < 4; ++j)
//             {
// 				glTexCoord2d(j < 2 ? 1.0f : 0.0f, j == 1 || j == 2 ? 1.0f : 0.0f);
// 				glVertex3d(tiles[i].vertices[j].x, tiles[i].vertices[j].y, tiles[i].vertices[j].z);
//             }
//
//             glColor4d(1,0,0,1);
//             for(j = 4; j < 8; ++j)
//             {
// 				glTexCoord2d(j < 6 ? 1.0f : 0.0f, j == 5 || j == 6 ? 1.0f : 0.0f);
// 				glVertex3d(tiles[i].vertices[j].x, tiles[i].vertices[j].y, tiles[i].vertices[j].z);
//             }
// 			glColor4d(1,1,1,1);
        }
    }

    glEnd();
	glPopMatrix();

	//uncomment this for textures
	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();
// 	glLoadIdentity();
// 	glMatrixMode(GL_MODELVIEW);

}

void L_Quit(void)
{
	glDeleteLists(tileList, 1);
	if(tile_tex) SDL_FreeSurface(tile_tex);
	if(special_tex) SDL_FreeSurface(special_tex);
	if(spec_format) free(spec_format);
	glDeleteTextures(1, &texIndex);
}

GLdouble L_GetTileHeight(int x, int z)
{
	int index;

	if(x > TILES_NUM_W)
	{
		x = TILES_NUM_W-1; //clamp to array bounds
	}

	if(z > TILES_NUM_H)
	{
		z = TILES_NUM_H-1;
	}

	index = (x*TILES_NUM_W)+z;

	if( index > TILEBUF_SIZE || index < 0)
	{
		return -1.0;
	}

	return tiles[x*TILES_NUM_W+z].h;
}

const L_tile_t* L_GetTileFromPlayerPos(const playerpos_t *player)
{
	long row, col;
	int index;
	L_tile_t * result;

	// 0.0 is the "middle" tile, or bottom right tile from midpoint if side lengths are even
	col = (long) trunc(player->x + (TILES_NUM_W*TILE_SIZE)/2);
	row = (long) trunc(player->z + (TILES_NUM_H*TILE_SIZE)/2);
	index = (row*TILES_NUM_H)+col;
	if( index > TILEBUF_SIZE || index < 0 )
	{
		result = &tiles[0];
	}
	else
	{
		result = &tiles[index];
	}
	return result;

}
