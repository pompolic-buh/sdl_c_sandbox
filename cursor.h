#ifndef CURSOR_H
#define CURSOR_H

#include <SDL/SDL_types.h>
#include <GL/gl.h>

typedef struct cursor2d_
{
	Uint16 x, y; /* 2D coordinates extracted from SDL_MouseMotionEvent */
} cursor2d_t;

typedef struct cursor3d_
{
	GLdouble xpos, ypos, zpos; // 3D coordinates
	GLdouble dir[3]; // vector pointing towards the plane -- equal to the inverse of the plane's normal
} cursor3d_t;

void		CUR_InitCursor(void);
// void		CUR_Update3DCursor(void);
void		CUR_UpdateCursorCoords(Uint16 x, Uint16 y);

cursor2d_t	CUR_Get2DCoords(void);
// void		CUR_Set2DCoords(Uint16 x, Uint16 y);

cursor3d_t	CUR_Get3DCoords(void);
// void		CUR_Set3DCoords(GLdouble x, GLdouble y, GLdouble z);
void		CUR_Draw3DCursor(void);

void		CUR_Quit(void);

#endif // CURSOR_H
