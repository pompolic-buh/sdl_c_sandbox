#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H
#include <GL/gl.h>

typedef void * RES_spritehandle;

int RES_Init();
void RES_Quit();

// Returns a handle to the sprite referred to by spritename (an internal identifier, derived from the filename)
// Loads the sprite and initialises refcount to 1 if not loaded yet
RES_spritehandle RES_GetSpriteHandle(const char * spritename);

// Increases the refcount of the sprite with given handle
void RES_LockSpriteByHandle(RES_spritehandle sprite);
// void RES_LockSpriteByName(const char * spritename);

// Decreases the refcount of the sprite with given handle, if it is <= 0, the sprite is destroyed
void RES_UnlockSpriteByHandle(RES_spritehandle sprite);
// void RES_UnlockSpriteByName(const char * spritename);

// Returns the sprite image associated to the given handle. Will need to be casted for the format appropriate for the implementation (e.g. SDL_Surface*)
// Returns NULL on error (invalid handle, sprite name not found, etc)
// TODO: default sprite image?
void* RES_ExtractSpriteImageByHandle(RES_spritehandle sprite);
// void* RES_ExtractSpriteImageByName(const char * spritename);

GLuint RES_ExtractGLTextureNameByHandle(RES_spritehandle sprite);

// void RES_FreeSprite(RES_spritehandle);


#endif // RESOURCEMANAGER_H
