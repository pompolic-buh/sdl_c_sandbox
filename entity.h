#ifndef ENTITY_H
#define ENTITY_H

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

typedef enum {
	ENT_SPRITE,
	ENT_MODEL,
	ENT_NODRAW
} ent_type_t;

typedef struct {
	ent_type_t type;
	GLdouble transform[16];
	GLdouble *vertices;
	GLuint buffer;
	//tex *tex;

} ent_entity_t;

ENT_Init();

ENT_MoveEntity(ent_entity_t* entity, double xdelta, double ydelta, double zdelta);

//ENT_SendToRenderer(ent_entity_t* entity);

ENT_Quit();

#endif //ENTITY_H
