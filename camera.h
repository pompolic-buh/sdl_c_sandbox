#ifndef CAMERA_H
#define CAMERA_H

#include <GL/gl.h>

typedef struct camerarot_ {
  GLdouble x;
  GLdouble y;
  GLdouble z;
} camerarot_t;

typedef struct camerapos_ {
  GLdouble x;//vert
  GLdouble y;//horiz
  GLdouble z;
  GLdouble horiz;
  GLdouble vert;
} camerapos_t;

typedef struct camera_ {
    camerarot_t rotation;
    camerapos_t position;
    GLdouble distance;
} camera_t;

int CAM_Init(void);

camerarot_t     CAM_GetCameraRotation(void);
void            CAM_RotateCamera(GLdouble xrot, GLdouble yrot, GLdouble zrot);

camerapos_t CAM_GetCameraPosition(void);
void        CAM_MoveCamera(GLdouble xdelta, GLdouble ydelta, GLdouble zdelta);

GLdouble		CAM_GetCameraDistance(void);
void		CAM_UpdateCameraDistance(GLdouble delta);

void CAM_Quit(void);

#endif //CAMERA_H
