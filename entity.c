#include "entity.h"

#include <GL/glext.h>

static GLuint sprite_vertex_buffer;
static GLdouble sprite_vertices[] = {
	-0.5, 0.0, 0.0,
	 0.5, 0.0, 0.0,
	 0.5, 2.0, 0.0,
	-0.5, 2.0, 0.0
};

ENT_Init()
{
	glGenBuffers(1, &sprite_vertex_buffer);
}

ENT_MoveEntity(ent_entity_t* entity, double xdelta, double ydelta, double zdelta)
{

}

ENT_Quit()
{
	glDeleteBuffers(1, &sprite_vertex_buffer);
}
