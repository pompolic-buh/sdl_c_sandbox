#ifndef MTH_H
#define MTH_H

#include <SDL/SDL_types.h>
#include <GL/gl.h>

#define MTH_PI 3.14159265

Uint16 MTH_gcd_Uint16(Uint16 a, Uint16 b);

#endif
