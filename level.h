#ifndef LEVEL_H
#define LEVEL_H

#include <GL/gl.h>
#include "player.h"
// struct playerpos_t;

#define TILES_NUM_W 6
#define TILES_NUM_H (TILES_NUM_W)

// rows/columns of tiles drawn, not counting the row/col playeris on
#define TILE_DRAW_RADIUS (TILES_NUM_W / 2)
#define TILE_SIZE 1.0

typedef struct L_vertex_
{
	GLdouble x, y, z;	// spatial coords
	GLdouble u, v;	// texture coords
} L_vertex_t;

typedef struct L_tile_
{
	L_vertex_t vertices[8];
	int show;	// tile is only drawn when this is nonzero
	GLdouble colors[3];
	// x, z are tile col/row, h is height
	int x, z;
	GLdouble h;
} L_tile_t;


// Load textures
void L_Init(void);
// Generate the level
void L_Generate(void);
// Generate tiles to draw from player position
void L_GenTiles(playerpos_t player_ur);
// Actually draw the tiles;
void L_DrawTiles(void);
// Cleanup unused tiles
void L_CullTiles(playerpos_t player_ur);
// Free resources etc
void L_Quit(void);

// Returns the height of block at given coordinates
GLdouble L_GetTileHeight(int x, int z);

const L_tile_t * L_GetTileFromPlayerPos(const playerpos_t* player);

#endif // LEVEL_H
