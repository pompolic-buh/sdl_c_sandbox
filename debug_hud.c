#include "debug_hud.h"

#include "player.h"
#include "cursor.h"
#include "constants.h"

#include <stdio.h>
#include <SDL/SDL_video.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <math.h>

static int DH_whattoprint = 0;
static int DH_wasTTFinitedhere = 0;

static SDL_Surface *HUD;
static TTF_Font *debug_font;
static GLuint fontTex;

static char msgbuf[256];

static char **stringbuf;

//TODO: make DH_ functions no-ops with ifdef trickery if the debug hud is not enabled
//      ensure they are only needed in this file

int DH_PowerOfTwo(int input);

int DH_Init(SDL_Surface* screen)
{
	if(!TTF_WasInit())
	{
		if(TTF_Init() == -1)
		{
			fprintf(stderr, "TTF_Init() failed! %s\n", TTF_GetError());
			return 1;
		}
		DH_wasTTFinitedhere = 1;
	}

		debug_font = TTF_OpenFont("fonts/debug.ttf", 18);
		if(!debug_font)
		{
			fprintf(stderr, "Failed to open fonts/debug.ttf\n");
			return 2;
		}

// 		HUD = SDL_CreateRGBSurface(SDL_HWSURFACE, D_screen->h, D_screen->w, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
// 		HUD = SDL_CreateRGBSurface(SDL_SRCALPHA, 400, 300, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
		HUD = SDL_CreateRGBSurface(SDL_SRCALPHA, IWIDTH, IHEIGHT, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
// 		HUD = DH_GetDebugHUDSurface();



		SDL_LockSurface(HUD);
		//Make a texture from the HUD surface
		glGenTextures(1, &fontTex);
		glBindTexture(GL_TEXTURE_2D, fontTex);
// 		glTexImage2D(GL_TEXTURE_2D, 0, 4, texw, texh, 0, GL_RGBA, GL_UNSIGNED_BYTE, HUD->pixels);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, HUD->w, HUD->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, HUD->pixels);
// 		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 200, 150, GL_RGBA, GL_UNSIGNED_BYTE, HUD->pixels);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glBindTexture(GL_TEXTURE_2D, 0);
		SDL_UnlockSurface(HUD);


		return 0;
}

void DH_Toggle(debug_show what)
{
	DH_whattoprint ^= what;
}

int DH_PowerOfTwo(int input)
{
	double log2 = log(input) / log(2);
	int log2i = (int) ceil(log2);
	return (int) (pow(2,log2i));
}

void DH_DrawAxes(GLdouble xrot, GLdouble yrot, GLdouble zrot)
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glTranslated(-4.0, -3.0, -15.0);
	glRotated(xrot, 1.0, 0.0, 0.0);
	glRotated(yrot, 0.0, 1.0, 0.0);
	glRotated(zrot, 0.0, 0.0, 1.0);


	glBegin(GL_LINES);
	glColor3d(1.0, 0.0, 0.0);
	glVertex3d(0.0, 0.0, 0.0);
	glVertex3d(1.0, 0.0, 0.0);

	glColor3d(0.0, 1.0, 0.0);
	glVertex3d(0.0, 0.0, 0.0);
	glVertex3d(0.0, 1.0, 0.0);

	glColor3d(0.0, 0.0, 1.0);
	glVertex3d(0.0, 0.0, 0.0);
	glVertex3d(0.0, 0.0, 1.0);
	glColor3d(1.0, 1.0, 1.0);
	glEnd();
	glPopMatrix();
}

void DH_WriteAt(Uint16 x, Uint16 y, const char* text)
{
	SDL_Rect text_pos;
	SDL_Color white = { 0xFF, 0xFF, 0xFF };
	SDL_Surface *text_surface;

	text_surface = TTF_RenderText_Solid(debug_font, text, white);

	text_pos.x = x;
	text_pos.y = y;
	text_pos.w = text_surface->w;
	text_pos.h = text_surface->h;

	SDL_FillRect(HUD, &text_pos, SDL_MapRGBA(HUD->format,255,255,0,25));
	SDL_BlitSurface(text_surface, NULL, HUD, &text_pos);

	SDL_FreeSurface(text_surface);
}

void DH_Print()
{
	char debug_buffer[256];
	int lineOffset;
	int texw, texh;

	// texture name for the text
	GLuint fontTex_local;

	SDL_Color white = { 0xFF, 0xFF, 0xFF };
	SDL_Color green = { 0x00, 0xFF, 0x00 };
	SDL_Surface *text_surface;
	SDL_Rect text_pos;

	text_pos.x = 0;
	text_pos.y = IHEIGHT -20;

	text_pos.w = IWIDTH;
	text_pos.h = 20;

	//FIXME: 0,255,255 is yellow -- RGBA/BGRA confusion somewhere

	if(DH_whattoprint & DEBUG_SHOW_POS)
	{
		playerpos_t pos = P_GetPlayerPos();

		snprintf(debug_buffer, 255, "x: %lf", pos.x);
		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);

		text_pos.y -= 20;
		snprintf(debug_buffer, 255, "y: %lf", pos.y);
		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);

		text_pos.y-= 20;
		snprintf(debug_buffer, 255, "z: %lf", pos.z);
		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);

		if(!HUD) return;
	}

	if(DH_whattoprint & DEBUG_SHOW_FACING)
	{
		playerfacing_t facing = P_GetPlayerFacing();

		snprintf(debug_buffer, 255, "xrot: %lf", facing.xrot);
		text_pos.y-= 20;
		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);


		snprintf(debug_buffer, 255, "yrot: %lf", facing.yrot);
		text_pos.y-= 20;
		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);

		snprintf(debug_buffer, 255, "zrot: %lf", facing.zrot);
		text_pos.y-= 20;
		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);

		if(!HUD) return;
	}

	if(DH_whattoprint & DEBUG_SHOW_DIRS)
	{
		snprintf(debug_buffer, 255, "oops DEBUG_SHOW_DIRS not implemented");
		text_pos.y-= 20;
		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);
		if(!HUD) return;
	}

	if(DH_whattoprint & DEBUG_SHOW_CURSOR)
	{
		cursor2d_t cursor2d = CUR_Get2DCoords();
		cursor3d_t cursor3d = CUR_Get3DCoords();

		snprintf(debug_buffer, 255, "mouse_x: %4.1d", cursor2d.x);
		text_pos.y-= 20;
		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);

		snprintf(debug_buffer, 255, "mouse_y: %4.1d", cursor2d.y);
		text_pos.y-= 20;
		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);

// 		sprintf(debug_buffer, "mouse3d_z: %f", cursor3d.zpos);
// 		text_pos.y-= 20;
// 		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);
//
// 		sprintf(debug_buffer, "mouse3d_y: %f", cursor3d.ypos);
// 		text_pos.y-= 20;
// 		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);
//
// 		sprintf(debug_buffer, "mouse3d_x: %f", cursor3d.xpos);
// 		text_pos.y-= 20;
// 		DH_WriteAt(text_pos.x, text_pos.y, debug_buffer);

		if(!HUD) return;
	}

	if (msgbuf)
	{
		text_surface = TTF_RenderText_Solid(debug_font, msgbuf, green);
		text_pos.y -= 80;
		text_pos.w = text_surface->w;
		SDL_FillRect(HUD, &text_pos, SDL_MapRGBA(HUD->format,0,0,255,25));
		SDL_BlitSurface(text_surface, NULL, HUD, &text_pos);
		SDL_FreeSurface(text_surface);
	}

	glBindTexture(GL_TEXTURE_2D, fontTex);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, HUD->w, HUD->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, HUD->pixels);



	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0,IWIDTH, 0, IHEIGHT, -1, 1);

	glMatrixMode(GL_MODELVIEW);

	glPushMatrix();
	glLoadIdentity();

	glBegin(GL_QUADS);
	glColor4f(1,1,1,1);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(0.0f, 0.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(IWIDTH, 0.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(IWIDTH, IHEIGHT, 0.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, IHEIGHT, 0.0f);
	glEnd();

	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	//FIXME: do something like this for 3d HUD objects

	/*
	//Draw the texture
	glDisable(GL_DEPTH_TEST);  // disabling depth test because the HUD would render behind the level
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glTranslated(0, 0, -32.0);
// 	glRotated(45.0, 1.0, 1.0, 0);
	glTranslated(-1.0, 0, 0);


	glBegin(GL_QUADS);
	glColor4f(1,1,1,1);
	// WIDTH, HEIGHT, gcd
	glTexCoord2f(0.0f, 1.0f); glVertex3f(0.0f, 0.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(25.0f, 0.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(25.0f, 18.75f, 0.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 18.75f, 0.0f);

	glEnd();
	glFinish();

	glTranslatef(0, 0, 70.0f);
	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
	*/

	glBindTexture(GL_TEXTURE_2D, 0);
}

void DH_DisplayMsg(const char* msg)
{
	strncpy(msgbuf, msg, 255);
	msgbuf[255] = '\0';
}

void DH_Quit()
{
	TTF_CloseFont(debug_font);
	glDeleteTextures(1, &fontTex);
	SDL_FreeSurface(HUD);

	if(DH_wasTTFinitedhere)
	{
		TTF_Quit();
	}
}

void DH_Clear()
{
	SDL_FillRect(HUD, NULL, SDL_MapRGBA(HUD->format,255,255,255,0));
}
