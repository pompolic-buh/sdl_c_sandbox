#include <stdio.h>
#include <math.h>

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "level.h"
#include "player.h"
#include "actor.h"
#include "cursor.h"
#include "camera.h"
#include "constants.h"

#include "debug_hud.h"

#define PIOVER180 0.0174532925f

void M_handle_keypress(SDL_keysym *sym)
{
	switch(sym->sym)
	{
		default:
			break;
	}
}

int main ( int argc, char **argv )
{
    static const Uint32 FPS = 60;
    Uint32 waittime = 1000.f/FPS;
    Uint32 framestarttime = 0;
    Sint32 delaytime;
    int done = 0;
	char ver[256];
	GLubyte *version;
	Uint8 *keystate;

	playerpos_t curr_plrpos;
	const L_tile_t * plrtile;
	camerarot_t curr_camerarot;
	GLdouble plr_height;
// 	GLdouble xrot_cam = 0, yrot_cam = 0, zrot_cam = 0;
	GLdouble zrot_vec_len = 1.0;
	GLdouble translate = -24.0;
	AC_actorindex_t testactor;

	SDL_Surface *fBuffer;
    SDL_Event currentEvent;

    if ( SDL_Init ( SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER ) < 0 )
	{
        fprintf ( stderr, "SDL failed to init! Reason: %s. Exiting.\n", SDL_GetError() );
        return 1;
    } else printf ( "SDL successfully initialized.\n" );

    if ( (fBuffer = SDL_SetVideoMode ( IWIDTH, IHEIGHT, 0, SDL_OPENGLBLIT )) == NULL )
	{
        fprintf ( stderr, "Failed to set video mode! Reason: %s. Exiting.\n", SDL_GetError() );
        SDL_Quit();
        return 2;
    } else
    {
        version = glGetString(GL_VERSION);
        printf ( "Video mode set to %dx%d, flags: 0x%.2x. OpenGL Version: %s\n", IWIDTH, IHEIGHT, fBuffer->flags, version );
    }

	if ( IMG_Init(IMG_INIT_PNG) == 0 )
	{
		fprintf(stderr, "Failed to initialize SDL_image! Reason: %s. Exiting.\n", SDL_GetError());
		SDL_Quit();
		return 3;
	} else printf( "SDL_image successfully initialized.\n");

	//OpenGL init stuff

	glViewport(0, 0, IWIDTH, IHEIGHT);		//TODO: resize callback
    glClearColor(0.0f, 0.0f, 0.1f, 0.0f);
    glClearDepth(1.0);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
//     glShadeModel(GL_FLAT);

    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
// 	glCullFace(GL_FRONT); // FIXME: use counterclockwise-wound polygons instead of clockwise-wound

	//FIXME: debug stuff
// 	glPointSize(4.0);
// 	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLdouble)IWIDTH/(GLdouble)IHEIGHT, 0.1f, 100.0f);

    glMatrixMode(GL_MODELVIEW);

    SDL_WM_SetCaption ( "OpenGL/SDL test thing", NULL );

	glTranslatef(0.0, 0.0, -24.0);
	glRotatef(45.0, 1.0, 0.0, 0.0);

	CAM_Init();
	P_InitPlayer();
	L_Init();
	L_Generate();
	CUR_InitCursor();
	RES_Init();
	AC_Init();
	testactor = AC_CreateActor();

#ifdef DEBUG_HUD
	if( DH_Init(fBuffer) )	// DEBUG
	{
		fprintf(stderr, "Failed to init Debug HUD!\n");
		SDL_Quit();
		return 10;
	}
 	//DH_Toggle(DEBUG_SHOW_DIRS | DEBUG_SHOW_FACING | DEBUG_SHOW_POS); // DEBUG
//	DH_Toggle(DEBUG_SHOW_POS);
//	DH_Toggle(DEBUG_SHOW_CURSOR);
	DH_DisplayMsg("*A*//    _I___I_");
#endif
    printf ( "Entering main loop.\n" );

    while ( !done ) {
        curr_plrpos = P_GetPlayerPos();
		curr_camerarot = CAM_GetCameraRotation();

		glClear ( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );

		glLoadIdentity();

		glTranslatef(0.0, 0.0, translate);
		//glRotatef(45.0, 1.0, 0.0, 0.0);

		glPushMatrix();
		glRotated(curr_camerarot.x, 1.0, 0.0, 0.0);
// 		glRotated(curr_camerarot.z, 0.0, 0.0, 1.0);
		glRotated(curr_camerarot.y, 0.0, 1.0, 0.0);

#ifdef DEBUG_HUD
		DH_DrawAxes(curr_camerarot.x, curr_camerarot.y, 0);
#endif // DEBUG_HUD

		glTranslated(-curr_plrpos.x, -curr_plrpos.y, -curr_plrpos.z);

// 		L_GenTiles(curr_plrpos);
		P_DrawPlayer();
		AC_DrawActors();
		L_DrawTiles();
		CUR_Draw3DCursor();
// 		L_CullTiles(curr_plrpos);

		glPopMatrix();

#ifdef DEBUG_HUD
		DH_Print();	// DEBUG
#endif

        SDL_GL_SwapBuffers();

#ifdef DEBUG_HUD
		DH_Clear();
#endif

        //Event processing
        while ( SDL_PollEvent ( &currentEvent ) ) {
            switch ( currentEvent.type ) {
            case SDL_KEYDOWN:
				M_handle_keypress(&currentEvent.key.keysym);
				break;
            case SDL_QUIT:
                done = 1;
                break;
			case SDL_MOUSEMOTION:
				CUR_UpdateCursorCoords(currentEvent.motion.x, currentEvent.motion.y);
				break;
            default:
                break;
            }
        }

        keystate = SDL_GetKeyState(NULL);
		if (keystate[SDLK_a] == SDL_PRESSED)
		{
			curr_plrpos = P_UpdatePlayerPos(-TILE_SIZE, 0.0, 0.0);
// 			P_UpdatePlayerFacing(0.0, 2.5, 0.0);
		}
		if (keystate[SDLK_d] == SDL_PRESSED)
		{
// 			P_UpdatePlayerFacing(0.0, -2.5, 0.0);
			curr_plrpos = P_UpdatePlayerPos(TILE_SIZE, 0.0, 0.0);
		}
		//FIXME: movement handling maybe in diff. file?
		if (keystate[SDLK_w] == SDL_PRESSED)
		{
// 			GLfloat xpos_delta, zpos_delta;

// 			P_UpdatePlayerPos( -((GLfloat) sin(facing.yrot*PIOVER180)* 0.1f), 0.0,  -((GLfloat) cos(facing.yrot*PIOVER180)* 0.1f));
			curr_plrpos = P_UpdatePlayerPos(0.0, 0.0, -TILE_SIZE);
		}
		if (keystate[SDLK_s] == SDL_PRESSED)
		{
// 			P_UpdatePlayerPos( (GLfloat) sin(facing.yrot*PIOVER180)* 0.1f, 0.0,  (GLfloat) cos(facing.yrot*PIOVER180)* 0.1f);
			curr_plrpos = P_UpdatePlayerPos(0.0, 0.0, TILE_SIZE);
		}
		if (keystate[SDLK_i] == SDL_PRESSED)
		{
			CAM_RotateCamera(0.5, 0, 0);
		}
		if (keystate[SDLK_k] == SDL_PRESSED)
		{
			CAM_RotateCamera(-0.5, 0, 0);
		}
		if (keystate[SDLK_j] == SDL_PRESSED)
		{
			CAM_RotateCamera(0, 0.5, 0);
		}
		if (keystate[SDLK_l] == SDL_PRESSED)
		{
			CAM_RotateCamera(0, -0.5, 0);
		}
		if (keystate[SDLK_u] == SDL_PRESSED)
		{
			CAM_RotateCamera(0, 0, 0.5);
		}
		if (keystate[SDLK_o] == SDL_PRESSED)
		{
			CAM_RotateCamera(0, 0, -0.5);
		}
		if (keystate[SDLK_t] == SDL_PRESSED)
		{
			AC_SetActorCoords(testactor, curr_plrpos.x, curr_plrpos.y, curr_plrpos.z);
		}
		if (keystate[SDLK_KP_PLUS] == SDL_PRESSED)
		{
			translate += 0.1;
			CAM_UpdateCameraDistance(-0.1);
		}
		if (keystate[SDLK_KP_MINUS] == SDL_PRESSED)
		{
			translate -= 0.1;
			CAM_UpdateCameraDistance(0.1);
		}

		plrtile = L_GetTileFromPlayerPos(&curr_plrpos);
		curr_plrpos = P_SetPlayerHeight(plrtile->h);

        //Go to sleep
        delaytime = waittime - ( SDL_GetTicks() - framestarttime );     //delay until next frame (base time minus time elapsed between frames)
        if ( delaytime > 0 )
            SDL_Delay ( ( Uint32 ) ( delaytime ) );          //why does this only work with casting
        framestarttime = SDL_GetTicks();
    }

    AC_Quit();
	RES_Quit();
	CUR_Quit();
	L_Quit();
	CAM_Quit();
#ifdef DEBUG_HUD
	DH_Quit();
#endif
	IMG_Quit();
    SDL_Quit();
    return 0;
}
