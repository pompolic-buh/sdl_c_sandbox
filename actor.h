#ifndef ACTOR_H
#define ACTOR_H

#include <GL/gl.h>

#include "resourcemanager.h"

// typedef struct AC_actorpos_
// {
// 	GLdouble x, y, z;
// } AC_actorpos_t;
//
// typedef struct actorfacing_
// {
// 	GLdouble xrot, yrot, zrot;
// } AC_actorfacing_t;
//
// typedef struct actor_
// {
// 	AC_actorpos_t pos;
// 	AC_actorfacing_t facing;
// 	//void *sprite;
// } AC_actor_t;

typedef unsigned int AC_actorindex_t;

// AC_actorpos_t AC_GetActorPos(const AC_actor_t *actor);
// void AC_UpdateActorCoords(AC_actor_t *actor, GLdouble xdelta, GLdouble ydelta, GLdouble zdelta);
void AC_SetActorCoords(AC_actorindex_t actor, GLdouble newx, GLdouble newy, GLdouble newz);

//TODO: 4-direction facing
// AC_actorfacing_t AC_GetActorFacing(const AC_actor_t *actor);
// void AC_UpdateActorFacing(AC_actor_t *actor, GLdouble xrot, GLdouble yrot, GLdouble zrot);

AC_actorindex_t AC_CreateActor();
// const AC_actor_t* AC_GetActor(AC_actorindex_t index);
void AC_SetActorSprite(AC_actorindex_t index, RES_spritehandle sprite);
void AC_DeleteActor(AC_actorindex_t index);

void AC_DrawActors();

int AC_Init();
void AC_Quit();

#endif //ACTOR_H
