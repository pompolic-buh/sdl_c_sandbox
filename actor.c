#include "actor.h"
#include <stdio.h>
#include <SDL/SDL_video.h>
#include <math.h>

#include "player.h"

#include "mth.h"
#include "camera.h"
#include "debug_hud.h"

#define AC_MAXACTORS 256

typedef struct AC_actorpos_
{
	GLdouble x, y, z;
} AC_actorpos_t;

typedef struct actorfacing_
{
	GLdouble xrot, yrot, zrot;
} AC_actorfacing_t;

typedef struct actor_
{
	AC_actorpos_t pos;
	AC_actorfacing_t facing;
// 	void *sprite;
	RES_spritehandle sprite;
} AC_actor_t;

static AC_actor_t actorbuffer[256];
static unsigned int actornum = 0;
static RES_spritehandle placeholder;

AC_actorpos_t AC_GetActorPos(const AC_actor_t* actor)
{
	return actor->pos;
}

void AC_UpdateActorCoords(AC_actor_t* actor, GLdouble xdelta, GLdouble ydelta, GLdouble zdelta)
{
	actor->pos.x += xdelta;
	actor->pos.y += ydelta;
	actor->pos.z += zdelta;
}

AC_actorfacing_t AC_GetActorFacing(const AC_actor_t* actor)
{
	return actor->facing;
}

void AC_UpdateActorFacing(AC_actor_t* actor, GLdouble xrot, GLdouble yrot, GLdouble zrot)
{
	actor->facing.xrot += xrot;
	actor->facing.yrot += yrot;
	actor->facing.zrot += zrot;
}

void AC_SetActorSprite(AC_actorindex_t index, RES_spritehandle sprite)
{
// 	SDL_Surface *spriteimage = (SDL_Surface*) RES_ExtractSpriteImageByHandle(sprite);

	RES_LockSpriteByHandle(sprite);
// 	actorbuffer[index].sprite = spriteimage;
	actorbuffer[index].sprite = sprite;
}

void AC_DrawActor(AC_actor_t* actor)
{
    GLdouble look[3];
    GLdouble right[3];
    GLdouble up[3];
    GLdouble rotation_matrix[16];
    GLdouble modelview[16];
    GLdouble look_length, right_length;
    GLdouble camerapos[4];
    GLdouble cameradist = CAM_GetCameraDistance();
    camerarot_t camerarot = CAM_GetCameraRotation();
    int i = 0;
    playerpos_t plr = P_GetPlayerPos();
    char camerapos_str[256];
    double horiz, vert, t;

	// TODO: use something other than immediate mode
// 	glPushAttrib(GL_POINT_SIZE);
// 	glPointSize(5);
	glPushMatrix();
	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

    //camerarot.y = horiz
    //camerarot.x = vert
    horiz = camerarot.y * MTH_PI / 180;
    vert = camerarot.x * MTH_PI / 180;

    //t = cameradist * cos(vert);

    //finally, the correct formula
    camerapos[0] = plr.x + cameradist * cos(vert) * -sin(horiz);
// 	camerapos[1] = plr.y;
    camerapos[1] = plr.y + cameradist * sin(vert);
// 	camerapos[2] = plr.z;
    camerapos[2] = plr.z + cameradist * cos(vert) * cos(horiz);
    camerapos[3] = 1;

    snprintf(camerapos_str, 255, "cam: %lf %lf %lf", camerapos[0], camerapos[1], camerapos[2]);
    DH_WriteAt(0, 400, camerapos_str);


//     camerapos[0] += plr.x * modelview[0] + plr.y * modelview[4] + plr.z * modelview[8] + modelview[12];
//     camerapos[1] += plr.x * modelview[1] + plr.y * modelview[5] + plr.z * modelview[9] + modelview[13];
//     camerapos[2] += plr.x * modelview[2] + plr.y * modelview[6] + plr.z * modelview[10] + modelview[14];
//     camerapos[3] += plr.x * modelview[3] + plr.y * modelview[7] + plr.z * modelview[11] + modelview[15];

	glDisable(GL_CULL_FACE);
	//vestigial billboarding go
	//look_length = sqrt(pow(camerapos[0] - actor->pos.x, 2) +  pow(camerapos[1] - actor->pos.y, 2) +  pow(camerapos[2] - actor->pos.z, 2));
//	look_length = sqrt(pow(actor->pos.x, 2)+pow(actor->pos.z, 2));

	look[0] = (camerapos[0]-(actor->pos.x));
	look[1] = 0.0;
	look[2] = (camerapos[2]-(actor->pos.z));

	look_length = sqrt(pow(camerapos[0] - actor->pos.x, 2) +  0.0 +  pow(camerapos[2] - actor->pos.z, 2));

	look[0] /= look_length;
	look[1] /= look_length;
	look[2] /= look_length;

	//up[0] = modelview[1];
	//up[1] = modelview[5];
	//up[2] = modelview[9];
	up[0] = 0.0;
	up[1] = 1.0;
	up[2] = 0.0;

	//right[0] = (1.0*look[2] - 0.0 * 0.0)*1.0
	right[0] = up[1] * look[2] - up[2] * look[1];
	right[1] = up[2] * look[0] - up[0] * look[2];
	right[2] = up[0] * look[1] - up[1] * look[0];

	rotation_matrix[0] = right[0];
	rotation_matrix[1] = right[1];
	rotation_matrix[2] = right[2];
	rotation_matrix[3] = 0.0;

	rotation_matrix[4] = up[0];
	rotation_matrix[5] = up[1];
	rotation_matrix[6] = up[2];
	rotation_matrix[7] = 0.0;

	rotation_matrix[8] = look[0];
	rotation_matrix[9] = look[1];
	rotation_matrix[10] = look[2];
	rotation_matrix[11] = 0.0;

	rotation_matrix[12] = actor->pos.x;
	rotation_matrix[13] = actor->pos.y;
	rotation_matrix[14] = actor->pos.z;
	rotation_matrix[15] = 1.0;

	glMultMatrixd(rotation_matrix);

    //64x32 images
	glBindTexture(GL_TEXTURE_2D, RES_ExtractGLTextureNameByHandle(placeholder));
	glBegin(GL_QUADS);
//	glTexCoord2d(0.0, 1.0); glVertex3d(actor->pos.x-0.5, actor->pos.y, actor->pos.z);
//	glTexCoord2d(1.0, 1.0); glVertex3d(actor->pos.x+0.5, actor->pos.y, actor->pos.z);
//	glTexCoord2d(1.0, 0.0); glVertex3d(actor->pos.x+0.5, actor->pos.y+2, actor->pos.z);
//	glTexCoord2d(0.0, 0.0); glVertex3d(actor->pos.x-0.5, actor->pos.y+2, actor->pos.z);
	glTexCoord2d(0.0, 1.0); glVertex3d(-0.5, 0.0, 0.0);
	glTexCoord2d(1.0, 1.0); glVertex3d(0.5, 0.0, 0.0);
	glTexCoord2d(1.0, 0.0); glVertex3d(0.5, 2.0, 0.0);
	glTexCoord2d(0.0, 0.0); glVertex3d(-0.5, 2.0, 0.0);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

    glEnable(GL_CULL_FACE);
	glPopMatrix();
// 	glPopAttrib();
}

void AC_DrawActors()
{
	GLfloat old_pointsize;
	GLdouble old_colors[4];
	glGetFloatv(GL_POINT_SIZE, &old_pointsize);
// 	glGetDoublev(GL_CURRENT_COLOR, old_colors);
	glPointSize(5.0);
// 	glColor4d(1.0, 0.0, 0.0, 1.0);
	unsigned int i = 0;
	for(i = 0; i < actornum; ++i)
	{
		AC_DrawActor(&actorbuffer[i]);
	}
	glPointSize(old_pointsize);
// 	glColor4dv(old_colors);
}


AC_actor_t *AC_GetActor(AC_actorindex_t index)
{
	if(index < AC_MAXACTORS)
	{
		return &(actorbuffer[index]);
	}
	else
	{
		return NULL;
	}
}

void AC_SetActorCoords(AC_actorindex_t actor, GLdouble newx, GLdouble newy, GLdouble newz)
{
	AC_actor_t *actor_ptr = AC_GetActor(actor);

	actor_ptr->pos.x = newx;
	actor_ptr->pos.y = newy;
	actor_ptr->pos.z = newz;
}

AC_actorindex_t AC_CreateActor()
{
	AC_actorindex_t act;
	if (actornum < AC_MAXACTORS)
	{
		actorbuffer[actornum].pos.x = 0;
		actorbuffer[actornum].pos.y = 0;
		actorbuffer[actornum].pos.z = 0;

		actorbuffer[actornum].facing.xrot = 0.0;
		actorbuffer[actornum].facing.yrot = 0.0;
		actorbuffer[actornum].facing.zrot = 0.0;
		act = actornum++;
		return act;
	}
	else
	{
		fprintf(stderr, "AC_CreateActor(): Ran out of actor slots! Reusing old ones now. Happy debugging.");
		return actornum;
	}
}

int AC_Init()
{
	actornum = 0;
	placeholder = RES_GetSpriteHandle("sprites/lemonbalm");
	return 0;
}

void AC_Quit()
{
	if(placeholder != NULL)
		RES_UnlockSpriteByHandle(placeholder);
}

void AC_DeleteActor(AC_actorindex_t index)
{
	//TODO: this
	//TODO: malloc() or keeping track of free actor slots
	// pointer to first free slot, arithmetic?
}
