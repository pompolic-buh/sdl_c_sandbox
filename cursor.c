#include "cursor.h"
#include "camera.h"
#include "player.h"
#include "debug_hud.h"

#include <GL/glu.h>

static cursor2d_t cursor2d;
static cursor3d_t cursor3d;

//TODO: header with common defines/typedefs
static GLdouble proj[16], model[16];
static GLint viewport[4];
static GLint cursorDL, redlineDL;


void CUR_InitCursor(void)
{
	/* we build a display list for the cursor */
	cursorDL = glGenLists(2);
	redlineDL = cursorDL + 1;
	glNewList(cursorDL, GL_COMPILE);
	glBegin(GL_QUADS);

	glColor4d(0.0, 1.0, 0.0, 0.5);

	/* front */
	glVertex3d( 0.5, -0.5,  0.5);
	glVertex3d( 0.5,  0.5,  0.5);
	glVertex3d(-0.5,  0.5,  0.5);
	glVertex3d(-0.5, -0.5,  0.5);

	/* right */
	glVertex3d( 0.5, -0.5, -0.5);
	glVertex3d( 0.5,  0.5, -0.5);
	glVertex3d( 0.5,  0.5,  0.5);
	glVertex3d( 0.5, -0.5,  0.5);

	/* back */
	glVertex3d(-0.5, -0.5, -0.5);
	glVertex3d(-0.5,  0.5, -0.5);
	glVertex3d( 0.5,  0.5, -0.5);
	glVertex3d( 0.5, -0.5, -0.5);

	/* bottom */
	glVertex3d(-0.5, -0.5,  0.5);
	glVertex3d(-0.5, -0.5, -0.5);
	glVertex3d( 0.5, -0.5, -0.5);
	glVertex3d( 0.5, -0.5,  0.5);

	/* left */
	glVertex3d(-0.5, -0.5,  0.5);
	glVertex3d(-0.5,  0.5,  0.5);
	glVertex3d(-0.5,  0.5, -0.5);
	glVertex3d(-0.5, -0.5, -0.5);

	/* top */
	glVertex3d( 0.5,  0.5,  0.5);
	glVertex3d( 0.5,  0.5, -0.5);
	glVertex3d(-0.5,  0.5, -0.5);
	glVertex3d(-0.5,  0.5,  0.5);

	glEnd();
	glEndList();

	glNewList(redlineDL, GL_COMPILE);
	glBegin(GL_LINES);
	glColor4d(1, 0, 0, 1);
	glVertex3d(0,  0, 0);
	glVertex3d(0, -9999, 0);
	glEnd();

	glEndList();
}

void CUR_Update3DCursor(void)
{
	GLdouble z;
	Sint16 y_adjusted;
	char line0[256], line1[256], line2[256], line3[256];

	glGetDoublev(GL_PROJECTION_MATRIX, proj);

	glPushMatrix();

	glGetDoublev(GL_MODELVIEW_MATRIX, model);

	snprintf(line0, 255, "%lf %lf %lf %lf", proj[0], proj[4], proj[8], proj[12]);
	snprintf(line1, 255, "%lf %lf %lf %lf", proj[1], proj[5], proj[9], proj[13]);
	snprintf(line2, 255, "%lf %lf %lf %lf", proj[2], proj[6], proj[10], proj[14]);
	snprintf(line3, 255, "%lf %lf %lf %lf", proj[3], proj[7], proj[11], proj[15]);

#ifdef DEBUG_HUD
	DH_WriteAt(0, 100, line0);
	DH_WriteAt(0, 120, line1);
	DH_WriteAt(0, 140, line2);
	DH_WriteAt(0, 160, line3);
#endif // DEBUG_HUD

	glPopMatrix();

	//TODO: only update viewport on resize
	glGetIntegerv(GL_VIEWPORT, viewport);

	//SDL window coordinates begin from the top left, OpenGL ones start at bottom left corner, so we need to reverse the direction y grows
	y_adjusted = viewport[3] - cursor2d.y;


	glReadPixels(cursor2d.x, y_adjusted, 1, 1, GL_DEPTH_COMPONENT, GL_DOUBLE, &z);
	gluUnProject(cursor2d.x, y_adjusted, z, model, proj, viewport, &(cursor3d.xpos), &(cursor3d.ypos), &(cursor3d.zpos));
}

// we must translate from SDL's coordinate system ((0,0) is top left corner) to the program's coordinate system (0,0,0) is in the center of 3d space
void CUR_UpdateCursorCoords(Uint16 x, Uint16 y)
{
	GLdouble z;

	cursor2d.x = x;
	cursor2d.y = y;
}

cursor2d_t CUR_Get2DCoords(void)
{
	return cursor2d;
}

void CUR_Set2DCoords(Uint16 x, Uint16 y)
{
	cursor2d.x = x;
	cursor2d.y = y;
}

cursor3d_t CUR_Get3DCoords(void)
{
	return cursor3d;
}

//TODO: 3d cursor should move along a plane parallel to the displayed plane
void CUR_Set3DCoords(GLdouble x, GLdouble y, GLdouble z)
{
	cursor3d.xpos = x;
	cursor3d.ypos = y;
	cursor3d.zpos = z;
}

void CUR_Draw3DCursor(void)
{
	GLdouble mat[16];
	GLdouble othermat[16];
	char line0[256], line1[256], line2[256], line3[256];
	int i = 0;
	playerpos_t curr_plrpos = P_GetPlayerPos();
	camerarot_t curr_camerarot = CAM_GetCameraRotation();

	glGetDoublev(GL_MODELVIEW_MATRIX, mat);

	snprintf(line0, 255, "%lf %lf %lf %lf", mat[0], mat[4], mat[8], mat[12]);
	snprintf(line1, 255, "%lf %lf %lf %lf", mat[1], mat[5], mat[9], mat[13]);
	snprintf(line2, 255, "%lf %lf %lf %lf", mat[2], mat[6], mat[10], mat[14]);
	snprintf(line3, 255, "%lf %lf %lf %lf", mat[3], mat[7], mat[11], mat[15]);

#ifdef DEBUG_HUD
	DH_WriteAt(0, 300, line0);
	DH_WriteAt(0, 320, line1);
	DH_WriteAt(0, 340, line2);
	DH_WriteAt(0, 360, line3);
#endif

	CUR_Update3DCursor();

	snprintf(line0, 255, "1 0 0 %lf", cursor3d.xpos);
	snprintf(line1, 255, "0 1 0 %lf", cursor3d.ypos);
	snprintf(line2, 255, "0 0 1 %lf", cursor3d.zpos);
	snprintf(line3, 255, "0 0 0 1");

#ifdef DEBUG_HUD
	DH_WriteAt(500, 200, line0);
	DH_WriteAt(500, 220, line1);
	DH_WriteAt(500, 240, line2);
	DH_WriteAt(500, 260, line3);
#endif
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	//TODO: glPushAttrib()
	glPointSize(5);

	glLoadIdentity();
	glTranslated(0,0,-24.0);
	glRotatef(45.0, 1.0, 0.0, 0.0);
	glRotated(curr_camerarot.x, 1.0, 0.0, 0.0);
	glRotated(curr_camerarot.y, 0.0, 1.0, 0.0);
	glRotated(curr_camerarot.z, 0.0, 0.0, 1.0);
	glTranslated(-curr_plrpos.x, -curr_plrpos.y, -curr_plrpos.z);
	glTranslated(cursor3d.xpos, cursor3d.ypos, cursor3d.zpos);
// 	glScaled(0.01, 0.01, 0.01);
//


	//multiply each with appropriate line
// 	glTranslated(mat[12], mat[13], mat[14]);
// 	glScaled(0.01,0.01,0.01);
// 	glTranslated(cursor3d.xpos, cursor3d.ypos, cursor3d.zpos);



	glGetDoublev(GL_MODELVIEW_MATRIX, othermat);


	//FIXME: do this but in Update3DCursor()
	othermat[12] *= 200;
	othermat[13] *= 200;
	othermat[14] *= 200;

	glLoadMatrixd(othermat);

	snprintf(line0, 255, "%lf %lf %lf %lf", othermat[0], othermat[4], othermat[8], othermat[12]);
	snprintf(line1, 255, "%lf %lf %lf %lf", othermat[1], othermat[5], othermat[9], othermat[13]);
	snprintf(line2, 255, "%lf %lf %lf %lf", othermat[2], othermat[6], othermat[10], othermat[14]);
	snprintf(line3, 255, "%lf %lf %lf %lf", othermat[3], othermat[7], othermat[11], othermat[15]);

#ifdef DEBUG_HUD
	DH_WriteAt(410, 300, line0);
	DH_WriteAt(410, 320, line1);
	DH_WriteAt(410, 340, line2);
	DH_WriteAt(410, 360, line3);
// 	DH_WriteAt(300, 440, line0);
// 	DH_WriteAt(300, 460, line1);
// 	DH_WriteAt(300, 480, line2);
// 	DH_WriteAt(300, 500, line3);
#endif
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glCallList(cursorDL);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


	//TODO: look into glPushAttrib()/glPopAttrib()
// 	glPushAttrib(GL_DEPTH_TEST | GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glColor4d(0.0, 1.0, 0.0, 1.0);
	glCallList(cursorDL);
// 	glPopAttrib();
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	glCallList(redlineDL);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glBegin(GL_POINTS);
	glColor3d(0.0, 1.0, 0.0);
	glVertex3d(0.0, 0.0, 0.0);
	glEnd();

	glPopMatrix();

	glPushMatrix();
	glBegin(GL_LINES);
	glColor3d(0.0, 1.0, 0.0);
	glVertex3d(cursor3d.xpos, cursor3d.ypos, cursor3d.zpos);
	// this only worked because those matrix elements have been always 0
	// try again
	glVertex3d(mat[3], mat[7], mat[11]);
// 	glVertex3d(cursor3d.xpos, mat[7], cursor3d.zpos);
	glEnd();

	glPointSize(1);
	glPopMatrix();
}

void CUR_Quit(void )
{
	glDeleteLists(cursorDL, 1);
}
