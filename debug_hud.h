#ifndef DEBUG_HUD_H
#define DEBUG_HUD_H

#include <SDL/SDL_video.h>
#include <GL/gl.h>

typedef enum {
	DEBUG_SHOW_POS = 1,
	DEBUG_SHOW_FACING = 2,
	DEBUG_SHOW_DIRS = 4,
	DEBUG_SHOW_CURSOR = 8
} debug_show;

// Initializes the debug HUD
int DH_Init(SDL_Surface* screen);

// Toggles display of an attribute
void DH_Toggle(debug_show what);

// Draws a graphic of the x,y,z axes in the level coord. system in the corner
void DH_DrawAxes(GLdouble xrot, GLdouble yrot, GLdouble zrot);

// Writes text at specified screen coordinates
void DH_WriteAt(Uint16 x, Uint16 y, const char * text);

// Prints debug information
void DH_Print();

// Schedules a string to be printed. Returns nonzero on failure.
void DH_DisplayMsg(const char * msg);

// Clear the surface (e.g. before drawing every frame)
void DH_Clear();

// Shuts down the debug HUD
void DH_Quit();

#endif // DEBUG_HUD_H
