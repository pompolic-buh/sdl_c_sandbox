#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "resourcemanager.h"

typedef struct RES_spritetuple_ RES_spritetuple_t;

//TODO: ideally, the rest of the code shouldn't even need the image pointers
//TODO: renderer

struct RES_spritetuple_ {
    char name[256];
    SDL_Surface *image;
	unsigned int refcount;
	GLuint texname;
    RES_spritetuple_t *next;
    RES_spritetuple_t *prev;
};

static RES_spritetuple_t *last;
static RES_spritetuple_t *first;

int RES_Init()
{
	last = first = NULL;
	return 0;
}

RES_spritetuple_t * RES_FindSprite(const char *spritename)
{
	//TODO: autoadd PNG
    //TODO: strncmp
	return NULL;
}

RES_spritehandle RES_GetSpriteHandle(const char *spritename)
{
	RES_spritetuple_t * sprite;
	SDL_Surface *img;
	char filename[256];

	sprite = RES_FindSprite(spritename);
	//spritename is the file name, without .png extension
	strncpy(filename, spritename, 255);
	filename[251] = '\0';
	strncat(filename, ".png", 4);

	// sprite not yet allocated
	if(sprite == NULL)
	{
		img = IMG_Load(filename);
		if(!img)
		{
			fprintf(stderr, "RES_GetSpriteHandle(): %s\n", IMG_GetError());
			return NULL;
		}

		// Allocate new list node for the sprite
		sprite = malloc(sizeof(RES_spritetuple_t));
		strncpy(sprite->name, spritename, 255);
		sprite->name[255] = '\0';
		sprite->refcount = 1;
		sprite->image = img;
		sprite->texname = 0;

		// now put it in the linked list
		if (last != NULL)
        {
            sprite->prev = last;
            last->next = sprite;
        }

		last = sprite;
		if (first == NULL)
        {
            first = sprite;
        }

        sprite->next = first;
		first->prev = sprite;

		return (RES_spritehandle) sprite;
	}
	// sprite is already allocated
	else
	{
		sprite->refcount++;
		return (RES_spritehandle) sprite;
	}
}

void RES_LockSpriteByHandle(RES_spritehandle sprite)
{
	RES_spritetuple_t *sp = (RES_spritetuple_t *) sprite;

	sp->refcount++;
}

void RES_LockSpriteByName(const char *spritename)
{
	RES_spritetuple_t *sprite;

	sprite = RES_FindSprite(spritename);
	if(sprite != NULL)
	{
		sprite->refcount++;
	}
}

void RES_UnlockSpriteByHandle(RES_spritehandle sprite)
{
	RES_spritetuple_t *sp = (RES_spritetuple_t *) sprite;
	RES_spritetuple_t *previous, *next;

	if ( --(sp->refcount) <= 0 )
	{
		SDL_FreeSurface(sp->image);

		previous = sp->prev;
		next = sp->next;
		next->prev = previous;
		previous->next = next;

		free(sp);
	}

}

void RES_UnlockSpriteByName(const char *spritename)
{
	RES_spritetuple_t *sp;
	RES_spritetuple_t *previous, *next;

	sp = RES_FindSprite(spritename);

	if (sp != NULL && --(sp->refcount) <= 0 )
	{
		SDL_FreeSurface(sp->image);

		previous = sp->prev;
		next = sp->next;
		next->prev = previous;
		previous->next = next;

		free(sp);
	}

}

void * RES_ExtractSpriteImageByHandle(RES_spritehandle sprite)
{
	if(sprite != NULL)
		return ((RES_spritetuple_t *)sprite)->image;
	else
		return NULL;
}

void * RES_ExtractSpriteImageByName(const char *spritename)
{
	RES_spritetuple_t *sprite;

	sprite = RES_FindSprite(spritename);
	if(sprite != NULL)
	{
		return sprite->image;
	}
	else
	{
		return NULL;
	}
}

GLuint RES_ExtractGLTextureNameByHandle(RES_spritehandle sprite)
{
	RES_spritetuple_t * sp = (RES_spritetuple_t *) sprite;

	if(sp->texname == 0)
	{
		glGenTextures(1, &(sp->texname));
		glBindTexture(GL_TEXTURE_2D, sp->texname);
		SDL_LockSurface(sp->image);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, sp->image->w, sp->image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, sp->image->pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
		SDL_UnlockSurface(sp->image);
	}

	return sp->texname;
}

void RES_Quit()
{
	//TODO: destroy all RES_spritetuple_t
}
