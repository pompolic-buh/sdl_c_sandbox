#include "player.h"
#include <math.h>

#define PIOVER180 0.0174532925f

static playerstate_t player1_state;

int P_InitPlayer(void )
{
// 	extern player1_state;

	player1_state.health = 100.0;
	player1_state.position.x = 0.0;
	player1_state.position.y = 0.0;
	player1_state.position.z = 0.0;

	return 0;
}

playerstate_t P_GetPlayerState(void )
{
	return player1_state;
}

playerpos_t P_GetPlayerPos(void )
{
// 	extern player1_state;

	return player1_state.position;
}

playerfacing_t P_GetPlayerFacing(void )
{
	return player1_state.facing;
}

playerfacing_t P_UpdatePlayerFacing(GLdouble xrot, GLdouble yrot, GLdouble zrot)
{
	player1_state.facing.xrot += xrot;
	player1_state.facing.yrot += yrot;
	player1_state.facing.zrot += zrot;

	return player1_state.facing;
}

playerpos_t P_UpdatePlayerPos(GLdouble xdelta, GLdouble ydelta, GLdouble zdelta)
{
// 	extern player1_state;

	player1_state.position.x += xdelta;
	player1_state.position.y += ydelta;
	player1_state.position.z += zdelta;

	return player1_state.position;
}

float P_GetPlayerHealth(void )
{
	return player1_state.health;
}

float P_UpdatePlayerHealth(float delta)
{
	return (player1_state.health += delta);
}

//FIXME: this is all debug stuff
//FIXME: remove math.h and the PIOVER180 macro

void P_DrawPlayer(void )
{
	GLdouble psize;
	glPushMatrix();
	glGetDoublev(GL_POINT_SIZE, &psize);
	glPointSize(4);
	glMatrixMode(GL_MODELVIEW);
	glBegin(GL_POINTS);
	glColor4d(0.0, 1.0, 0.0, 1.0);
	glVertex3f(player1_state.position.x, player1_state.position.y, player1_state.position.z);
// 	glColor4d(1.0, 0.0, 0.0, 1.0);
// 	glVertex3f(player1_state.position.x- sin(player1_state.facing.yrot*PIOVER180), player1_state.position.y, player1_state.position.z-cos(player1_state.facing.yrot*PIOVER180));
	glColor4d(1.0, 1.0, 1.0, 1.0);
	glEnd();
	glPointSize(psize);
	glPopMatrix();
}

playerpos_t P_SetPlayerHeight(GLdouble newy)
{
	player1_state.position.y = newy;
	return player1_state.position;
}

